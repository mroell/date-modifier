//
//  AppDelegate.h
//  Date Modifier
//
//  Created by Matthias Roell on 26.11.14.
//  Copyright (c) 2014 Matthias Roell. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (weak) IBOutlet NSWindow *window;

#pragma mark Settings

@property (weak) IBOutlet NSDatePicker *datePicker;
@property (weak) IBOutlet NSDatePicker *timePicker;

@property (weak) IBOutlet NSTextField *hourOffsetField;
@property (weak) IBOutlet NSStepper *hourOffsetStepper;

@property (weak) IBOutlet NSTextField *minuteOffsetField;
@property (weak) IBOutlet NSStepper *minuteOffsetStepper;

@property (weak) IBOutlet NSTextField *secondOffsetField;
@property (weak) IBOutlet NSStepper *secondOffsetStepper;

@property (weak) IBOutlet NSButton *createdFlag;
@property (weak) IBOutlet NSButton *modifiedFlag;

@property (weak) IBOutlet NSButton *hiddenFileFlag;
@property (weak) IBOutlet NSButton *subDirectoryFlag;

@property (weak) IBOutlet NSTokenField *fileTypeField;

@property (weak) IBOutlet NSMatrix *modeMatrix;

- (IBAction)hiddenFileFlagChange:(NSButton *)sender;
- (IBAction)modeAction:(NSMatrix *)sender;

#pragma mark Logs & Path

@property (weak) IBOutlet NSPathControl *pathControl;

@property (weak) IBOutlet NSTextField *summaryLog;
@property (unsafe_unretained) IBOutlet NSTextView *changeLog;

@property (weak) IBOutlet NSButton *modifyButton;

- (IBAction)pathChangedAction:(NSPathControl *)sender;
- (IBAction)modifyFilesAction:(NSButton *)sender;

#pragma mark Misc

- (IBAction)print:(id)sender;

@end

