//
//  main.m
//  Date Modifier
//
//  Created by Matthias Roell on 26.11.14.
//  Copyright (c) 2014 Matthias Roell. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
