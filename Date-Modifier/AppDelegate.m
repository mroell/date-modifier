//
//  AppDelegate.m
//  Date Modifier
//
//  Created by Matthias Roell on 26.11.14.
//  Copyright (c) 2014 Matthias Roell. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate () {
    NSFont * defaultFont;
    NSFont * boldFont;
}

@end

@implementation AppDelegate

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender {
    return YES;
}

#pragma mark Public Methods

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [self.pathControl setURL:[NSURL URLWithString:NSHomeDirectory()]];
    
    defaultFont = [NSFont fontWithName:@"Menlo" size:11];
    boldFont = [NSFont fontWithName:@"Menlo-Bold" size:11];
    
    // Date Picker Setup (Current Date)
    [self.datePicker setDateValue:[NSDate date]];
    [self.timePicker setDateValue:[NSDate date]];
    
    // Offset Setup (1 Hour Offset)
    [self.hourOffsetField setEnabled:NO];
    [self.hourOffsetStepper setEnabled:NO];
    [self.hourOffsetField setIntegerValue:1];
    
    [self.minuteOffsetField setEnabled:NO];
    [self.minuteOffsetStepper setEnabled:NO];
    [self.minuteOffsetField setIntegerValue:0];
    
    [self.secondOffsetField setEnabled:NO];
    [self.secondOffsetStepper setEnabled:NO];
    [self.secondOffsetField setIntegerValue:0];
    
    // Tooltip Settings
    [self.datePicker setToolTip:NSLocalizedString(@"datePickerTooltip", nil)];
    [self.timePicker setToolTip:NSLocalizedString(@"timePickerTooltip", nil)];
    
    [self.hourOffsetField setToolTip:NSLocalizedString(@"hourOffsetFieldTooltip", nil)];
    [self.minuteOffsetField setToolTip:NSLocalizedString(@"minuteOffsetFieldTooltip", nil)];
    [self.secondOffsetField setToolTip:NSLocalizedString(@"secondOffsetFieldTooltip", nil)];
    
    [self.createdFlag setToolTip:NSLocalizedString(@"createdFlagTooltip", nil)];
    [self.modifiedFlag setToolTip:NSLocalizedString(@"modifiedFlagTooltip", nil)];
    
    [self.hiddenFileFlag setToolTip:NSLocalizedString(@"hiddenFileFlagTooltip", nil)];
    [self.subDirectoryFlag setToolTip:NSLocalizedString(@"subDirectoryFlagTooltip", nil)];
    
    [self.fileTypeField setToolTip:NSLocalizedString(@"fileTypeFieldTooltip", nil)];
    
    [self.modeMatrix setToolTip:NSLocalizedString(@"modeMatrixTooltip", nil)];
    
    [self.pathControl setToolTip:NSLocalizedString(@"pathControlTooltip", nil)];
    
    // Update Summary Log
    [self _updateSummary];
}

- (IBAction)hiddenFileFlagChange:(NSButton *)sender {
    [self _updateSummary];
}

- (IBAction)modeAction:(NSMatrix *)sender {
    if (sender.selectedRow == 0) {
        [self.datePicker setEnabled:YES];
        [self.datePicker setAlphaValue:1.0];
        [self.timePicker setEnabled:YES];
        [self.timePicker setAlphaValue:1.0];
        
        [self.hourOffsetField setEnabled:NO];
        [self.hourOffsetStepper setEnabled:NO];
        [self.minuteOffsetField setEnabled:NO];
        [self.minuteOffsetStepper setEnabled:NO];
        [self.secondOffsetField setEnabled:NO];
        [self.secondOffsetStepper setEnabled:NO];
    } else {
        [self.datePicker setEnabled:NO];
        [self.datePicker setAlphaValue:0.5];
        [self.timePicker setEnabled:NO];
        [self.timePicker setAlphaValue:0.5];
        
        [self.hourOffsetField setEnabled:YES];
        [self.hourOffsetStepper setEnabled:YES];
        [self.minuteOffsetField setEnabled:YES];
        [self.minuteOffsetStepper setEnabled:YES];
        [self.secondOffsetField setEnabled:YES];
        [self.secondOffsetStepper setEnabled:YES];
    }
}

- (IBAction)pathChangedAction:(NSPathControl *)sender {
    [self _updateSummary];
    [self.changeLog.textStorage setAttributedString:[[NSAttributedString alloc] initWithString:@""]];
}

- (IBAction)modifyFilesAction:(NSButton *)sender {
    NSAlert * alert = [[NSAlert alloc] init];
    
    [alert setAlertStyle:NSWarningAlertStyle];
    [alert setIcon:[NSImage imageNamed:NSImageNameCaution]];
    [alert setMessageText:NSLocalizedString(@"AlertMessageText", nil)];
    [alert setInformativeText:NSLocalizedString(@"AlertInformativeText", nil)];
    
    [alert addButtonWithTitle:NSLocalizedString(@"AlertCancel", nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"AlertOK", nil)];
    
    [alert beginSheetModalForWindow:self.window completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSAlertSecondButtonReturn) {
            if (self.createdFlag.state == NSOnState || self.modifiedFlag.state == NSOnState){
                [self.changeLog.textStorage setAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\n", NSLocalizedString(@"StartingMessage", nil)] attributes:@{NSFontAttributeName: boldFont}]];
                
                if (self.createdFlag.state == NSOnState) {
                    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"ChangingCreatedFlagMessage", nil)] attributes:@{NSFontAttributeName: defaultFont}]];
                }
                if (self.modifiedFlag.state == NSOnState) {
                    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"ChangingModifiedFlagMessage", nil)] attributes:@{NSFontAttributeName: defaultFont}]];
                }
                
                if ([[self.modeMatrix cellAtRow:0 column:0] state] == NSOnState) {
                    [self _doDateChangeAction];
                } else {
                    [self _doTimeOffsetAction];
                }
            } else {
                [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\t", NSLocalizedString(@"NothingToDoMessage", nil)] attributes:@{NSFontAttributeName: defaultFont}]];
                [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\n", NSLocalizedString(@"NoFlagsMessage", nil)] attributes:@{NSFontAttributeName: boldFont}]];
                
                [self _updateChangeLog];
            }
        } else {
            [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"UserAbortMessage", nil) attributes:@{NSFontAttributeName: boldFont}]];
            
            [self _updateChangeLog];
        }
    }];
    
    
}

#pragma mark Misc Methods

- (void)print:(id)sender {
    [self.changeLog print:sender];
}

#pragma mark Private Methods

- (void)_updateChangeLog {
    [self.changeLog scrollToEndOfDocument:self];
}

- (void)_updateSummary {
    // ------------------------------------------------------------------------------------------------------------------------------------
    NSMutableAttributedString * logText = [[NSMutableAttributedString alloc] init];
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    
    // ------------------------------------------------------------------------------------------------------------------------------------
    NSAttributedString * folderString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\t", NSLocalizedString(@"SumCurrentFolderMessage", nil)]
                                                                        attributes:@{NSFontAttributeName: boldFont}];
    
    NSAttributedString * pathString = [[NSAttributedString alloc] initWithString:self.pathControl.URL.path
                                                                      attributes:@{NSFontAttributeName: defaultFont}];
    
    [logText appendAttributedString:folderString];
    [logText appendAttributedString:pathString];
    
    // ------------------------------------------------------------------------------------------------------------------------------------
    if ([fileManager isWritableFileAtPath:self.pathControl.URL.path]) {
        [self.modifyButton setEnabled:YES];
        
        NSUInteger fileCount = 0;
        NSUInteger dirCount = 0;
        
        NSDirectoryEnumerationOptions options = NSDirectoryEnumerationSkipsPackageDescendants | NSDirectoryEnumerationSkipsSubdirectoryDescendants;
        
        if (self.hiddenFileFlag.state == NSOffState) {
            options |= NSDirectoryEnumerationSkipsHiddenFiles;
        }
        
        NSEnumerator * enumerator = [fileManager enumeratorAtURL:self.pathControl.URL
                                      includingPropertiesForKeys:nil
                                                         options:options
                                                    errorHandler:^BOOL(NSURL *url, NSError *error) {
                                                        [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", error]
                                                                                                                                           attributes:@{NSFontAttributeName: boldFont}]];
                                                        return NO;
                                                    }];
        
        if (enumerator) {
            NSURL * item;
            BOOL isDirectory = NO;
            
            while (item = [enumerator nextObject]) {
                [fileManager fileExistsAtPath:[item path] isDirectory:&isDirectory];
                if (isDirectory) {
                    dirCount++;
                } else {
                    fileCount++;
                }
            }
            
            NSAttributedString * fileString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\n%@\n\t", NSLocalizedString(@"SumFileCountMessage", nil)]
                                                                              attributes:@{NSFontAttributeName: boldFont}];
            
            NSAttributedString * fileCountString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld", fileCount]
                                                                                   attributes:@{NSFontAttributeName: defaultFont}];
            
            NSAttributedString * directoryString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\n%@\n\t", NSLocalizedString(@"SumDirCountMessage", nil)]
                                                                                   attributes:@{NSFontAttributeName: boldFont}];
            
            NSAttributedString * directoryCountString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld", dirCount]
                                                                                        attributes:@{NSFontAttributeName: defaultFont}];
            
            [logText appendAttributedString:fileString];
            [logText appendAttributedString:fileCountString];
            [logText appendAttributedString:directoryString];
            [logText appendAttributedString:directoryCountString];
        }
    } else {
        NSMutableAttributedString * roString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\n%@", NSLocalizedString(@"SumReadOnlyMessage", nil)]
                                                                                      attributes:@{NSFontAttributeName: defaultFont}];
        
        [roString beginEditing]; {
            [roString addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(12, 9)];
        } [roString endEditing];
        
        [self.modifyButton setEnabled:NO];
        
        [logText appendAttributedString:roString];
    }
    
    // ------------------------------------------------------------------------------------------------------------------------------------
    [self.summaryLog setAttributedStringValue:logText];
}

- (void)_doDateChangeAction {
    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@ ->\t", NSLocalizedString(@"DateChangeModeMessage", nil)] attributes:@{NSFontAttributeName: boldFont}]];
    
    NSDateComponents * dateComp = [[NSCalendar currentCalendar] components:kCFCalendarUnitYear | kCFCalendarUnitMonth | kCFCalendarUnitDay fromDate:[self.datePicker dateValue]];
    NSDateComponents * timeComp = [[NSCalendar currentCalendar] components:kCFCalendarUnitHour | kCFCalendarUnitMinute | kCFCalendarUnitSecond fromDate:[self.timePicker dateValue]];
    
    [dateComp setHour:timeComp.hour];
    [dateComp setMinute:timeComp.minute];
    [dateComp setSecond:timeComp.second];
    
    NSDate * newDate = [[NSCalendar currentCalendar] dateFromComponents:dateComp];
    
    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\n", newDate] attributes:@{NSFontAttributeName: defaultFont}]];
    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"EnumerationTimeMessage", nil)] attributes:@{NSFontAttributeName: boldFont}]];
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    
    NSDirectoryEnumerationOptions options = 0;
    
    if (self.hiddenFileFlag.state == NSOffState) {
        options |= NSDirectoryEnumerationSkipsHiddenFiles;
    }
    if (self.subDirectoryFlag.state == NSOffState) {
        options |= NSDirectoryEnumerationSkipsHiddenFiles;
        options |= NSDirectoryEnumerationSkipsPackageDescendants;
    }
    
    NSArray * fileTypes = [[self.fileTypeField stringValue] componentsSeparatedByString:@","];
    
    if ([fileTypes count] == 1 && [[fileTypes lastObject] isEqualToString:@""]) {
        fileTypes = @[];
    }
    
    NSEnumerator * enumerator = [fileManager enumeratorAtURL:self.pathControl.URL
                                  includingPropertiesForKeys:nil
                                                     options:options
                                                errorHandler:^BOOL(NSURL *url, NSError *error) {
                                                    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", error]
                                                                                                                                       attributes:@{NSFontAttributeName: boldFont}]];
                                                    return NO;
                                                }];
    
    if (enumerator) {
        NSURL * item;
        NSMutableDictionary * fileAttributes;
        
        while (item = [enumerator nextObject]) {
            if ([fileTypes count] > 0) {
                if (![fileTypes containsObject:item.pathExtension]) {
                    continue;
                }
            }
            fileAttributes = [[fileManager attributesOfItemAtPath:item.path error:nil] mutableCopy];
            if (self.modifiedFlag.state == NSOnState) {
                [fileAttributes setObject:newDate forKey:NSFileModificationDate];
            }
            if (self.createdFlag.state == NSOnState) {
                [fileAttributes setObject:newDate forKey:NSFileCreationDate];
            }
            [fileManager setAttributes:fileAttributes ofItemAtPath:item.path error:nil];
            
            [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"->\t%@\n", item.path] attributes:@{NSFontAttributeName: defaultFont}]];
            [self _updateChangeLog];
        }
        
        [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@\n", NSLocalizedString(@"JobCompletedMessage", nil)] attributes:@{NSFontAttributeName: boldFont}]];
        [self _updateChangeLog];
    }
}

- (void)_doTimeOffsetAction {
    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@ ->\t", NSLocalizedString(@"TimeOffsetModeMessage", nil)] attributes:@{NSFontAttributeName: boldFont}]];
    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%02ld:%02ld:%02ld\n\n",
                                                                                                   self.hourOffsetStepper.integerValue,
                                                                                                   self.minuteOffsetStepper.integerValue,
                                                                                                   self.secondOffsetStepper.integerValue]
                                                                                       attributes:@{NSFontAttributeName: defaultFont}]];
    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"EnumerationTimeMessage", nil)] attributes:@{NSFontAttributeName: boldFont}]];
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    
    NSDirectoryEnumerationOptions options = 0;
    
    if (self.hiddenFileFlag.state == NSOffState) {
        options |= NSDirectoryEnumerationSkipsHiddenFiles;
    }
    if (self.subDirectoryFlag.state == NSOffState) {
        options |= NSDirectoryEnumerationSkipsHiddenFiles;
        options |= NSDirectoryEnumerationSkipsPackageDescendants;
    }
    
    NSArray * fileTypes = [[self.fileTypeField stringValue] componentsSeparatedByString:@","];
    
    if ([fileTypes count] == 1 && [[fileTypes lastObject] isEqualToString:@""]) {
        fileTypes = @[];
    }
    
    NSEnumerator * enumerator = [fileManager enumeratorAtURL:self.pathControl.URL
                                  includingPropertiesForKeys:nil
                                                     options:options
                                                errorHandler:^BOOL(NSURL *url, NSError *error) {
                                                    [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", error]
                                                                                                                                       attributes:@{NSFontAttributeName: boldFont}]];
                                                    return NO;
                                                }];
    
    if (enumerator) {
        NSURL * item;
        NSMutableDictionary * fileAttributes;
        
        NSDate * oldDate;
        
        NSInteger   hours = self.hourOffsetStepper.integerValue,
        minutes = self.minuteOffsetStepper.integerValue,
        seconds = self.secondOffsetStepper.integerValue;
        
        NSTimeInterval offset = seconds + (minutes * 60) + (((hours < 0) ? (hours) : (hours * -1)) * 3600);
        
        if (hours < 0) {
            offset = offset * -1;
        }
        
        while (item = [enumerator nextObject]) {
            if ([fileTypes count] > 0) {
                if (![fileTypes containsObject:item.pathExtension]) {
                    continue;
                }
            }
            fileAttributes = [[fileManager attributesOfItemAtPath:item.path error:nil] mutableCopy];
            if (self.modifiedFlag.state == NSOnState) {
                oldDate = [fileAttributes objectForKey:NSFileModificationDate];
                [fileAttributes setObject:[oldDate dateByAddingTimeInterval:offset] forKey:NSFileModificationDate];
            }
            if (self.createdFlag.state == NSOnState) {
                oldDate = [fileAttributes objectForKey:NSFileCreationDate];
                [fileAttributes setObject:[oldDate dateByAddingTimeInterval:offset] forKey:NSFileCreationDate];
            }
            [fileManager setAttributes:fileAttributes ofItemAtPath:item.path error:nil];
            
            [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"->\t%@\n", item.path] attributes:@{NSFontAttributeName: defaultFont}]];
            [self _updateChangeLog];
        }
        
        [self.changeLog.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@\n", NSLocalizedString(@"JobCompletedMessage", nil)] attributes:@{NSFontAttributeName: boldFont}]];
        [self _updateChangeLog];
    }
}

@end
